<?php
/**
 * Created by PhpStorm.
 * User: Supun Ishara
 * Date: 5/7/2018
 * Time: 10:32 PM
 */

namespace Tests\Unit\UnsplashImage;

use App\imageGallery\UnsplashImage\Repositories\UnsplashImageRepository;
use App\ImageGallery\UnsplashImage\UnsplashImage;
use Tests\TestCase;

class UnsplashImageUnitTest extends TestCase
{

    /** @test */
    public function it_can_delete_the_carousel()
    {
        $unsplashImage = factory(UnsplashImage::class)->create();

        $unsplashImageRepository = new UnsplashImageRepository($unsplashImage);
        $delete = $unsplashImageRepository->deleteUnsplashImage();

        $this->assertTrue($delete);
    }


    /** @test */
    public function it_can_update_the_unsplashImage()
    {
        $unsplashImage = factory(UnsplashImage::class)->create();

        $data = [
            'image_id' => $this->faker->word,
            'width' => $this->faker->number,
            'height' => $this->faker->number,
            'color' => $this->faker->hexColor,
            'regularImageURL' => $this->faker->url,
            'favouriteStatus' => $this->faker->boolean,
            'description' => $this->faker->word,
        ];

        $unsplashImageRepository = new UnsplashImageRepository($unsplashImage);
        $update = $unsplashImageRepository->updateUnsplashImage($data);

        $this->assertTrue($update);
        $this->assertEquals($data->image_id, $unsplashImage->image_id);
        $this->assertEquals($data->width, $unsplashImage->width);
        $this->assertEquals($data->height, $unsplashImage->height);
        $this->assertEquals($data->color, $unsplashImage->color);
        $this->assertEquals($data->regularImageURL, $unsplashImage->regularImageURL);
        $this->assertEquals($data->favouriteStatus, $unsplashImage->favouriteStatus);
        $this->assertEquals($data->description, $unsplashImage->description);
    }

    /** @test */
    public function it_can_show_the_unsplashImage()
    {
        $unsplashImage = factory(UnsplashImage::class)->create();
        $unsplashImageRepository = new UnsplashImageRepository(new UnsplashImage);
        $found = $unsplashImageRepository->findUnsplashImage($unsplashImage->id);

        $this->assertInstanceOf(UnsplashImage::class, $found);
        $this->assertEquals($found->image_id, $unsplashImage->image_id);
        $this->assertEquals($found->width, $unsplashImage->width);
        $this->assertEquals($found->height, $unsplashImage->height);
        $this->assertEquals($found->color, $unsplashImage->color);
        $this->assertEquals($found->regularImageURL, $unsplashImage->regularImageURL);
        $this->assertEquals($found->favouriteStatus, $unsplashImage->favouriteStatus);
        $this->assertEquals($found->description, $unsplashImage->description);
    }

    /** @test
     * @throws \Exception
     */
    public function it_can_create_a_unsplashImage()
    {
        $data = [
            'image_id' => $this->faker->word,
            'width' => $this->faker->number,
            'height' => $this->faker->number,
            'color' => $this->faker->hexColor,
            'regularImageURL' => $this->faker->url,
            'favouriteStatus' => $this->faker->boolean,
            'description' => $this->faker->word,
        ];

        $unsplashImageRepo = new UnsplashImageRepository(new UnsplashImage);
        $unsplashImage = $unsplashImageRepo->createUnsplashImage($data);

        $this->assertInstanceOf(UnsplashImage::class, $unsplashImage);
        $this->assertInstanceOf($data['image_id'], $unsplashImage->image_id);
        $this->assertInstanceOf($data['width'], $unsplashImage->width);
        $this->assertInstanceOf($data['height'], $unsplashImage->height);
        $this->assertInstanceOf($data['color'], $unsplashImage->color);
        $this->assertInstanceOf($data['regularImageURL'], $unsplashImage->regularImageURL);
        $this->assertInstanceOf($data['favouriteStatus'], $unsplashImage->favouriteStatus);
        $this->assertInstanceOf($data['description'], $unsplashImage->description);
    }
}